You can remove created K3d cluster created with this command:

[source,shell]
----
k3d cluster delete bookCluster
----

You can clean up your docker engine with this command:

[source,shell]
----
docker system prune -a --volumes
----

All tools installed with a help of the `brew` can be uninstalled with the command `brew uninstall` plus `tool name`.

[source,shell]
----
brew uninstall tool-name
----