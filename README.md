# Developing microservices - Frontend

## Checklist v1.0

- [x] development documentation
- [x] development with docker-compose
- [x] sign in - Vue.js
- [x] books list - Vue.js
- [ ] NGiNX proxy simulating ingress - both API and BFF should be on the same domain as otherwise some other examples planned later will not work
- [ ] HTTPS with https://github.com/cloudflare/cfssl
- [ ] Using `/etc/hosts` by creatign dev domain targeting 127.0.0.1
- [ ] HTTPS example required
- [ ] migration to the TypeScript
- [ ] sign up - Vue.js
- [ ] administration section - Vue.js
- [ ] react.js version analysis
- [ ] development with k3d
- [ ] fully working frontend microservice book-frontend example in Vue.js
- [ ] modernisation of the React.js version of the service to fit standards of local development set in the Vue.js version
- [ ] more ... to be specified later
