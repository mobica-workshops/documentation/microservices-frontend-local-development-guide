#!/usr/bin/env bash
mkdir public
cp -R images public/
cp microservices-frontend-local-development-guide.html public/index.html
cp microservices-frontend-local-development-guide.pdf public/
